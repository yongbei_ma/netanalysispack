'''
@author yongbeima
@date 2016.02.24
usage:python energy.py network_filename node_number noise(from 0.001 to 0.99)
'''
from ctypes import *
import numpy
import gc
import sys, string
import os
import scipy.sparse.linalg as la
import itertools
import random
from multiprocessing import Pool
from scipy import stats

#calculate the different position of two integer
def difpos(x,y,length):
  z =  x^y
  pos_list = []
  temp = 1
  for pos in range(length):
    if z & temp :
      pos_list.append(pos)
    temp <<= 1
  return pos_list

#convert position to state
def cvtsta(pos,state):
  state_list = [] #don't contant the beginnig state
  for p in pos:
    state = state - (1<<p) if (state>>p) % 2 else state + (1<<p)
    state_list.append(state)
  state_list.pop()
  return state_list;

#calculate the energy
def calengy(tmatrix,num_states):
  tmatrix = numpy.transpose(numpy.reshape(tmatrix, (num_states, num_states)))

  eval, evec = la.eigs(tmatrix, 1, sigma=1, which = 'LM')
  evec = numpy.abs(numpy.real(evec))
  if 0.0 in evec:
    eval, evec = la.eigs(tmatrix, 1, sigma=1, which="LM")
    evec = numpy.abs(numpy.real(evec))

  evec = -numpy.log(evec/numpy.sum(evec))
  return evec[:,0]

#calculate the T+noise matrix
def caltmatrix(net,N,num_entries,alpha):
  net = numpy.reshape(net, (1,N*N))
  pn=net.ctypes.data_as(c_void_p)
  tmatrix = numpy.zeros(shape=(1, num_entries))
  c_alpha = c_double(alpha)
  pa=tmatrix.ctypes.data_as(c_void_p)
  #get a c function
  func.gen(pn, pa, N, num_states,c_alpha)
  return tmatrix

#calculate the barrier,can change definition
def calbarrier(start_state,middle_states,end_state,energys):
  all_barrier = []
  pathpos = itertools.permutations(difpos(start_state,end_state,N))
  if len(difpos(start_state,end_state,N)) < 2:
    return 0
  for ppos in pathpos:
    state_path = cvtsta(ppos,start_state)
    if True in [sp in middle_states for sp in state_path]:
      continue
    all_barrier.append(max(energys[i] for i in state_path))

  return max(all_barrier)-energys[start_state]


#main program  
sys.path.append(os.getcwd())
# func = CDLL(os.getcwd()+"/transition.dll")
func = CDLL(os.getcwd()+"/transition.so")

f = open(sys.argv[1], 'r')
N = int(sys.argv[2])
alpha = float(sys.argv[3])

num_states = 2**N
num_entries = num_states**2

data = []
for line in f:
  col = line.split()
  if len(col) == N:
    data.append([float(i) for i in col])
f.close()

#main program
net = data[0:0+N]

tmatrix = caltmatrix(net,N,num_entries,alpha)
energys = calengy(tmatrix,num_states)

all_barrier = []
#######################################################
# compute average barrier height for budding
# uncomment this code to get barrier of budding
mainchain_bud = [1092,836,964,896,904,907,155,17,116,100,68]
mainchainLen_bud = 9
for i in range(mainchainLen_bud):
  for j in range(i+2,mainchainLen_bud+2):
    BH = calbarrier(mainchain_bud[i],mainchain_bud[i+1:j],mainchain_bud[j],energys)
    all_barrier.append(abs(BH))
#######################################################

#######################################################
# compute average barrier height for fission
# uncomment this code to get barrier of fission
# mainchain_fission = [356,4,132,130,138,154,19,101,100]
# mainchainLen_fission = 7
# for i in range(mainchainLen_fission):
#   for j in range(i+2,mainchainLen_fission+2):
#     BH = calbarrier(mainchain_fission[i],mainchain_fission[i+1:j],mainchain_fission[j],energys)
#     all_barrier.append(abs(BH))
#######################################################

#######################################################
# compute average barrier height for fission12
# uncomment this code to get barrier of fission12
# mainchain_fission12 = [2148,1892,4,132,146,27,125,117,101,100]
# mainchainLen_fission12 = 8
# for i in range(mainchainLen_fission12):
#   for j in range(i+2,mainchainLen_fission12+2):
#     BH = calbarrier(mainchain_fission12[i],mainchain_fission12[i+1:j],mainchain_fission12[j],energys)
#     all_barrier.append(abs(BH))
#######################################################

print numpy.mean(all_barrier)

gc.collect()



