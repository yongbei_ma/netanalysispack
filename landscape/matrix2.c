/***************************************************************************
 *
 * Authors: "Yongbei(Glow) Ma"
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This complete copyright notice must be included in any revised version of the
 * source code. Additional authorship citations may be added, but existing
 * author citations must be preserved.
 ***************************************************************************/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

//other help function
int inverseNum(int num,int length){
	int *N = (int*)malloc(length*sizeof(int));
	int i,result = 0;
	//convert to 2-bit number
	for(i = 0;i<length;i++){
		N[i] = num % 2;
		num = num / 2;
	}
	for (i = 0; i <length; i++)
		result = result*2 + N[i];
	free(N);
	return result;
}


//calcualte energy help function
void Dec2Bi(int * S, int Seed, int num_nodes)
{
	int i=0;
	//the binary order inverse in (S1,S2,S3,....)
	//Seed = inverseNum(Seed,num_nodes);
	for ( i = num_nodes-1; i >= 0; i-- )
	{
		S[i]=Seed&1;
		Seed>>=1;
	}
	//modify order
	//for ( i = 0; i < num_nodes; i++ )
	//{
		//S[i]=Seed&1;
		//Seed>>=1;
	//}
}

double TransProbability( double alpha, int SNow, int SNext, int num_nodes, int **A/**[num_nodes][num_nodes]**/)
{
	int i, t, sn; 
	//int S[num_nodes], SN[num_nodes];
	int *S = (int *)malloc(sizeof(int)*num_nodes);
	int *SN = (int *)malloc(sizeof(int)*num_nodes);
	for(i = 0;i<num_nodes;i++){
		S[i] = 0;
		SN[i] = 0;
	}
	Dec2Bi(S, SNow, num_nodes);
	Dec2Bi(SN, SNext, num_nodes);
	//double TotalIn, pr[num_nodes], Prob=1;
	double TotalIn,Prob=1;
	double *pr = (double *)malloc(sizeof(double)*num_nodes);
	for(i = 0;i<num_nodes;i++)
		pr[i] = 0;

	for ( i = 0; i < num_nodes; i++ )
	{
		TotalIn = 0.0;
		for ( t=0; t<num_nodes; t++)
				TotalIn += A[t][i] * S[t];
		
		if (TotalIn > 0)
			sn = 1;
		else if (TotalIn < 0)
			sn = 0;
		else if(A[i][i] == 1)  //modify,added
			sn = 1;
		else
			sn = S[i];

		pr[i] = (SN[i] == sn ? 1 - alpha : alpha);
		Prob *= pr[i];
	}
	free(S);
	free(SN);
	free(pr);
	return Prob;
}

int gen(const double* name, double* result, int num_nodes, int num_states, double alpha)
{
	double TotalIn;
	int i, j, t, s;
	//int A[num_nodes][num_nodes];
	int **A = (int **)malloc(num_nodes*sizeof(int *));
 	for(i=0;i<num_nodes;i++)
     	A[i] = (int *)malloc(num_nodes*sizeof(int));
	for(i = 0;i<num_nodes;i++)
    	for(j = 0;j<num_nodes;j++)
			A[i][j] = 0;

	//double T[num_states][num_states];
    double **T = (double **)malloc(num_states*sizeof(double *));
 	for(i=0;i<num_states;i++)
     	T[i] = (double *)malloc(num_states*sizeof(double));
    for(i = 0;i<num_states;i++)
    	for(j = 0;j<num_states;j++)
			T[i][j] = 0;

	//int S[num_nodes];
	int *S = (int *)malloc(num_nodes*sizeof(int));
	for(i = 0;i<num_nodes;i++)
		S[i] = 0;
	//int i, j, t, s;

	for (i = 0; i < num_nodes; ++i){
		for (j = 0; j < num_nodes; ++j){
			A[i][j] = (int) name[j * num_nodes + i];  //A[i][j]:i->j interaciton
			if (i != j)
			{
				if (A[i][j] > 0)
					A[i][j] *= 2;
				else if (A[i][j] < 0)
					A[i][j] *= 22;
			}
		}
	}

	for ( i = 0; i < num_states; i++ )
	{

		for ( j = 0; j < num_states; j++)	
		{
			
			T[i][j] = TransProbability(alpha, i, j, num_nodes, A);
		}
	}

	for (i=0; i<num_states; i++){
		for (j=0; j<num_states; j++){
			result[i*num_states+j]=T[i][j];
		}
	}
	for(i=0;i<num_nodes;i++)
		free(A[i]);
	free(A);
	for(i=0;i<num_states;i++)
		free(T[i]);
	free(T);
	free(S);
	return 0;
}
