##########################################

compute the energy of the network dynamics
@author YongBei.Ma
@date:2016.02.24
##########################################

1)complie the matrix2.c file:
in Linux/Mac:
gcc -fPIC -shared matrix.c -o transition.so
in windows:
gcc -fPIC -shared matrix.c -o transition.dll

2)run python compute the energy,
in energy.py code,make sure the function call is right,in Linux/Mac:
func = CDLL(os.getcwd()+"/transition.so")
in Windows:
func = CDLL(os.getcwd()+"/transition.dll")
the run and compute:
python energy.py network_filename node_number noise(from 0.001 to 0.99)
example:
python energy.py budding_full.txt 11 0.01
you will see the result print.

3)copy the pyhon result to matlab,use landscape.m to plot the result

##########################################