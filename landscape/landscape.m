%%%%%%%%% compute GRR  %%%%%%%%%%%%%
[f,xi] = ksdensity(U);
maxP = max(f);
averageU = 0;
halfwidthU = 0;
for i = 1:length(xi)
    if f(i) > 0.5*maxP && halfwidthU == 0
        halfwidthU = xi(i);
    end
    if f(i) == maxP && averageU == 0
        averageU = xi(i);
    end
end
averageU
halfwidthU
min(xi)
%%%%%%%%% plot density %%%%%%%%%%%%
% [f,xi] = ksdensity(U);
% plot(f,xi,'black');
% xlabel('Probability')
% ylabel('U')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%% plot all Z line %%%%%%%%%%%%
% for i = 1:size(U)
%     hold on;
%     h = plot([0,1],[U(i),U(i)],'r')
%     set(h, 'color', [0.5 0.5 0.5])
% end
% ylim([0,16])
% ylabel('U')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%  plot budding global surface  %%%%%%%%%
% Index = zeros(2025,3);
% % set X and Y
% Index(1:2025,1)=ceil([1:2025]/45);
% Index(1:2025,2)=mod([1:2025]-1,45)+1;
% % compute R
% Index(1:2025,3)=sqrt((Index(1:2025,1)-23).*(Index(1:2025,1)-23)+(Index(1:2025,2)-23).*(Index(1:2025,2)-23));
% % sort by R
% [V,I] =sort(Index(:,3));
% U_sort = sort(U);
% U_adjust = zeros(45,45);
% % manually order the minimum energy
% order_manual_N = 800
% for i = 1:order_manual_N
%     U_adjust(Index(I(i),1),Index(I(i),2)) = U_sort(i)+rand()*2-1;
% end
% % randomly order
% U_random = randperm(length(U_sort(order_manual_N+1:2025)))+order_manual_N
% for i = order_manual_N+1:2025
%     U_adjust(Index(I(i),1),Index(I(i),2)) = U_sort(U_random(i-order_manual_N));
% end
% surf(U_adjust)

% Interpolant 2D surface
% [X,Y] = meshgrid(1:size(U_adjust,2), 1:size(U_adjust,1));
% F = scatteredInterpolant(X(:), Y(:), U_adjust(:), 'linear');
% [U_interp,V] = meshgrid(linspace(1,size(U_adjust,2),100), linspace(1,size(U_adjust,1),100));
% surf(U_interp,V, F(U_interp,V),'EdgeColor','none')
% zlabel('U')
% colormap(jet)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
