'''
@author yongbeima
@date 2014.02.24
usage:python energy.py network_filename node_number noise(from 0.001 to 0.99)
'''
from ctypes import *
import numpy
import gc
import sys, string
import os
import scipy.sparse.linalg as la
import itertools
import random
from multiprocessing import Pool

#calculate the energy
def calengy(tmatrix,num_states):
  tmatrix = numpy.transpose(numpy.reshape(tmatrix, (num_states, num_states)))

  eval, evec = la.eigs(tmatrix, 1, sigma=1, which = 'LM')
  evec = numpy.abs(numpy.real(evec))
  if 0.0 in evec:
    eval, evec = la.eigs(tmatrix, 1, sigma=1, which="LM")
    evec = numpy.abs(numpy.real(evec))

  evec = -numpy.log(evec/numpy.sum(evec))
  return evec[:,0]

#calculate the T+noise matrix
def caltmatrix(net,N,num_entries,alpha):
  net = numpy.reshape(net, (1,N*N))
  pn=net.ctypes.data_as(c_void_p)
  tmatrix = numpy.zeros(shape=(1, num_entries))
  c_alpha = c_double(alpha)
  pa=tmatrix.ctypes.data_as(c_void_p)
  #get a c function
  func.gen(pn, pa, N, num_states,c_alpha)
  return tmatrix

#main program  
sys.path.append(os.getcwd())
func = CDLL(os.getcwd()+"/transition.so")
# cal = CDLL(os.getcwd()+"/cal.dll")
f = open(sys.argv[1], 'r')
N = int(sys.argv[2])
alpha = float(sys.argv[3])

num_states = 2**N
num_entries = num_states**2

data = []
for line in f:
  col = line.split()
  if len(col) == N:
    data.append([float(i) for i in col])
f.close()


st =  0
net = data[st:st+N]

tmatrix = caltmatrix(net,N,num_entries,alpha)
energys = calengy(tmatrix,num_states)

for i in range(num_states):
  print energys[func.inverseNum(i,N)]

gc.collect()



