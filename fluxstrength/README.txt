##########################################

software package to compute some properties of network
@author YongBei.Ma
@date:2016.02.24
##########################################

@NOTICE : each *.cpp file is independent,so you can use some c++ complier to build it,exmaple:
icpc checkpoints.cpp -std=c++11 -o checkpoint

@Description for each cpp file:
1)checkpoint.cpp,this is the main program to calculate the flux control strength
build:g++ checkpoint.cpp -std=c++11 -o checkpoint
usage:Usage: ./checkpoint fullnetwork  min-network  nodeSize  fixpoint
example:
for budding:./checkpoint budding_full.txt budding_min.txt 11 272
for fission:./checkpoint fission_full.txt fission_min.txt 9 76

2)process_network_dyn.cpp,this is the main program to calculate the network dynamics
build:icpc process_network_dyn.cpp -std=c++11 -o process_network_dyn
Usage: ./process_network_dyn fullnetwork  nodeSize  start-state(decimalism)
example:
for budding:./process_network_dyn budding_full.txt 11 273 > bud_dyn.net
the result contain two thing,one is the process step from 273(10001000100) dynamical state,
another is Vertices and Arcs data you can inport to GEPHI.

3)findallnetwork.cpp,enumerate all possible network from your given process
build:icpc findallnetwork.cpp -std=c++11 -o findallnetwork
Usage: ./findallnetwork network-process  nodeSize  period-time  threshold
budding example:
./findallnetwork budding_process_11_12.txt 11 12 12
the threshold is the links edges need to consider for each node,result is *node_num*.txt file,
each file contain all possible for this node.

4)pickperticularnetwork.cpp.After you run the findallnetwork,you get all possible links for each node,
then run pickperticularnetwork,that will pick some network with same minimum network,that uses to research 
minimum network with different supplement links.
build:icpc pickperticularnetwork.cpp -std=c++11 -o pickperticularnetwork
Usage: ./pickperticularnetwork   min-network  nodeNum 
budding example:./pickperticularnetwork budding_min.txt 11
the result is *node_num*_V.txt file

5)genetwork.cpp,After you pick the suiatble network(get the *node_num*_V.txt file),then run this,
you can get 1000 network *network*.txt file.
build:icpc gennetwork.cpp -o gennetwork -std=c++11
Usage: ./gennetwork  nodeNum
budding example:./gennetwork 11

6)gennetwork2.cpp.this is same as genetwork.cpp.it will also calculate the basia size and traffic weight of each 
network.
**for build this code make sure you have comment the main() function in checkpoint.cpp file ***

7)randomnet.cpp,this is used to find the main trajectory of the given network.

##########################################