/***************************************************************************
 *
 * Authors: "Yongbei(Glow) Ma"
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This complete copyright notice must be included in any revised version of the
 * source code. Additional authorship citations may be added, but existing
 * author citations must be preserved.
 ***************************************************************************/
/****for calculate the b,w,detB,detD
 **@author YongBei.Ma
 **@date 2013.08.27
 **@version 1.0.0
 *****/
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include <ctime>

#include <array>
#include <stack>
#include <vector>
#include <list>
#include <unordered_map>
#include <tuple>

#include <cstdlib>
#include <ctime>
#include <climits>

#include "../header/basicmatrix.h"
#include "../header/basicsequence.h"
#include "../header/helpfun.h"

/******************************calculate the dynamical network************************/

//calculate the next end state 
void nextEndState(int *network,Sequence& start_state,Sequence& end_state,int size)
{
  int temp;

  /****forbid domination model****/
  for(int i = 0;i < size;i++){//for every onde i
    temp = 0;     //result
    for(int j = 0;j < size;j++) //for all j
      if(start_state[j]){  //j is active
  	if(network[i*size+j] == -1){
  	  if(j != i) {
  	    temp = -100; //strong inhibitory,special case
  	    break;
  	  }
  	}
      	temp += network[i*size+j];
      }

    if(temp < 0)
      end_state[i] = 0;
    //!!!!! add && network[i*size+i] != 1 condition !!!!!!
    else if(temp == 0 && network[i*size+i] != 1)
      end_state[i] = start_state[i];
    else
      end_state[i] = true;
  }

   /*********boolean model********/
  // for(int i = 0;i < size;i++){//for every onde i
  //   temp = 0;     //result
  //   for(int j = 0;j < size;j++) //for all j
  //     if(start_state[j])
  //     	temp += (network[i*size+j]==-1)?-1:network[i*size+j]; //b = (a==0)?1:2

  //   if(temp < 0)
  //     end_state[i] = 0;
  //   else if(temp == 0)
  //     end_state[i] = start_state[i];
  //   else
  //     end_state[i] = true;
  // }
}

//give a network,and generate its corresponding DN:dynamical-network or SN:state-network
std::unordered_multimap<int,std::pair<int,int>> genDN(int *network,int size)
{
  bool set[2] = {false,true}; //it is useless
  Sequence start_state(size,2);
  Sequence end_state(size,2);

  std::unordered_multimap<int,std::pair<int,int>> stateNetwork;
  int stateSum = pow(2,size);
  for(int i = 0;i < size;i++)
    start_state[i] = true;
  for(int stateNum = 0;stateNum < stateSum;stateNum++){
    //calculate the end state
    nextEndState(network,start_state,end_state,size);
    //store it to a map:start_state->(end_state,b)
    stateNetwork.insert(std::unordered_multimap<int,std::pair<int,int>>::value_type 
			((int)end_state,std::make_pair((int)start_state,0)));
    //for every starting state,get the next state
    start_state.nextSequence();
  }  
  return stateNetwork;
}

/***************************************************************************************/


/***************************************calculate the B & w*****************************/

//calculate B recursion function
int calBRec(std::unordered_multimap<int,std::pair<int,int>> &SN,int start)
{
  int b = 1,temp;
  auto its = SN.equal_range(start);
  for (auto it = its.first; it != its.second; ++it)
    if(it->first != it->second.first){
      temp = calBRec(SN,it->second.first);
      b += temp;
      it->second.second = temp;//traffic flow
    }
  return b;
}

//calculate the <Wn>
double calWRec(std::unordered_multimap<int,std::pair<int,int>> &SN,
	       int start,int L,int total_trafic)
{
  int total_traffic2;
  double w = 0;
  auto its = SN.equal_range(start);
  for (auto it = its.first; it != its.second; ++it)
    if(it->first != it->second.first){
      //total traffic flow from it->second.second to the attractor
      total_traffic2 = total_trafic + it->second.second;
      w += (double)total_traffic2/L + calWRec(SN,it->second.first,L+1,total_traffic2);
    }
  return w;
}

//calculate the given dynamical-network's basin:B and its Wn
std::pair<int,double> calBW(std::unordered_multimap<int,std::pair<int,int>> &SN,int endState)
{
  int maxB = 0,temp,attractor = 0;
  double Wn;
  for(auto it = SN.begin(); it != SN.end(); it++){
    if(it->first == it->second.first && it->first == endState){
      temp = calBRec(SN,it->second.first);
      if(temp > maxB){
	maxB = temp;
	attractor = it->first;//main attractor
      }
    }
  }
  Wn = calWRec(SN,attractor,1,0);
  return std::make_pair(maxB,Wn/maxB);
}

//calculate the given dynamical-network's maxminum B and its corresponding w
std::pair<int,double> calmaxBW(std::unordered_multimap<int,std::pair<int,int>> &SN)
{
  int maxB = 0,temp,attractor = 0;
  double Wn;
  for(auto it = SN.begin(); it != SN.end(); it++){
    if(it->first == it->second.first){
      temp = calBRec(SN,it->second.first);
      if(temp > maxB){
	maxB = temp;
	attractor = it->first;//main attractor
      }
    }
  }
  Wn = calWRec(SN,attractor,1,0);
  return std::make_pair(maxB,Wn/maxB);
}

//calculate the given dynamical-network's maxminum-B's corresponding attractor
int findMaxAttractor(std::unordered_multimap<int,std::pair<int,int>> &SN)
{
  int maxB = 0,temp,attractor = 0;
  double Wn;
  for (int i = 0; i < 512; ++i)
  {
    std::cout<<(i+1)<<" \""<<(i+1)<<"\""<<std::endl;
  }
  for(auto it = SN.begin(); it != SN.end(); it++){
    std::cout<<(it->second.first+1)<<" "<<(it->first+1)<<std::endl;
    if(it->first == it->second.first){
      temp = calBRec(SN,it->second.first);
      //std::cout<<"temp = "<<temp<<std::endl;
      if(temp > maxB){
        maxB = temp;
        attractor = it->first;//main attractor
      }
    }
  }
  return attractor;
}

//pick the main trajectory with the maxminum B
void  findMainAttractor(std::unordered_multimap<int,std::pair<int,int>> &SN,std::list<int> &trajectory,int attractor)
{
  int maxB = 0,attractorTemp = attractor;
  //std::cout<<attractorTemp<<std::endl;
  trajectory.push_back(attractor);
  auto its = SN.equal_range(attractorTemp);
  for (auto it = its.first; it != its.second; ++it){
    if(it->first != it->second.first){
      if(it->second.second > maxB){
        maxB = it->second.second;
        attractorTemp = it->second.first;
      }
    }
  } 
  if(attractorTemp != attractor) findMainAttractor(SN,trajectory,attractorTemp);
}

/********************************************************************************************/


/***********************************calculate the detB and detD******************************/

//store every state-network node's B in "TF_B",has something wrong
int calDeltaBRec(std::unordered_multimap<int,std::pair<int,int>> &SN,int start,double *TF_B)
{
  int b = 1,temp;
  auto its = SN.equal_range(start);
  for (auto it = its.first; it != its.second; ++it)
    if(it->first != it->second.first){
      temp = calDeltaBRec(SN,it->second.first,TF_B);
      b += temp;
    }
  TF_B[start] = b;
  return b;
}

// int calDeltaBRec(std::unordered_multimap<int,std::pair<int,int>> &SN,int start,double *TF_B)
// {
//   int b = 0,temp;
//   auto its = SN.equal_range(start);
//   for (auto it = its.first; it != its.second; ++it){
//     if(it->first != it->second.first){
//       temp = calDeltaBRec(SN,it->second.first,TF_B);
//       if(temp) b += temp;
//       else b += 1;
//     }
//     else
//       b += 1;
//   }
//   TF_B[start] = b;
//   return b;
// }

//store every state-network node's D in "TF_D"
int calDeltaDRec(std::unordered_multimap<int,std::pair<int,int>> &SN,int start,double *TF_D)
{
  int d = 0,temp;
  auto its = SN.equal_range(start);
  for (auto it = its.first; it != its.second; ++it)
    if(it->first != it->second.first){
      temp = calDeltaDRec(SN,it->second.first,TF_D);
      d += 1;
      it->second.second = temp;//traffic flow
    }
  TF_D[start] = d;
  return d;
}

//a new method to calculate the new-parameter
int calDeltaPRec(std::unordered_multimap<int,std::pair<int,int>> &SN,int start,double *NP,int *level)
{
  int large_level = 0,count = 0,levelTemp = 0;
  double np = 0;
  auto its = SN.equal_range(start);
  for (auto it = its.first; it != its.second; ++it)
    if(it->first != it->second.first){
      levelTemp = calDeltaPRec(SN,it->second.first,NP,level);
      if(large_level < levelTemp){
	large_level = levelTemp;
	//std::cout<<"hello"<<std::endl;
      }
      np += levelTemp * NP[it->second.first];
      count++;
    }
  np = (np + count)/(large_level+1);
  NP[start] = np;
  if(count == 0) level[start] = -1;//large_level;
  else level[start] = large_level + 1;
  if(count == 0) return large_level;
  else return large_level+1;
}

//calculate the new parameter
double* calBD2(int *network,int size,int fixpoint)
{
  auto stateNetwork = genDN(network,size);

  int stateSum = pow(2,size);
  Sequence start_state(size,2);
  double *np = new double [stateSum];
  int *level = new int [stateSum];
  //initialization
  for (int i = 0; i < stateSum; ++i)
  {
    np[i] = 0;
    level[i] = 0;
  }

  calDeltaPRec(stateNetwork,fixpoint,np,level); //!!!!!
  // output the result 2016.02.04
  // for (int i = 0; i < stateSum; ++i)
  // {
  //   std::cout<<(np[i])/187*35+5<<std::endl;
  // }
  //statistics the checkpoint
  double *weight_p1 = new double[size];
  double *weight_p2 = new double[size];
  double *weight_p = new double[size];

  //initialization
  for (int i = 0; i < size; ++i)
  {
    weight_p1[i] = 0;
    weight_p2[i] = 0;
    weight_p[i] = 0;
  }
  int state;
  //calculate the p
  start_state.reset();
  for(int i = 0; i < stateSum;i++){
    state = (int)start_state;
    if(np[state] > 0){
      for(int j = 0; j < size; j++){
	if(start_state[j] == 1)
	  weight_p1[j] += np[state];
	else
	  weight_p2[j] -= np[state];
	
	weight_p[j] += np[state];
      }
      // std::cout<<np[state]<<" "<<level[state]<<std::endl;
    }
    start_state.nextSequence();
  }

  double *bd = new double [size];
  for(int j = 0; j < size; ++j){
    bd[j] = (weight_p1[j]+weight_p2[j])/weight_p[j];
    std::cout<<bd[j]<<std::endl;
    // std::cout<<weight_p1[j]<<" "<<weight_p2[j]<<" "<<weight_p[j]<<std::endl;
  }

  delete weight_p1;
  delete weight_p2;
  delete weight_p;
  delete np;
  delete level;
  return bd;
}

//calculate the delta-B & delta-D for a given network
double* calBD(int *network,int size,int fixpoint)
{
  auto stateNetwork = genDN(network,size);

  int stateSum = pow(2,size);
  Sequence start_state(size,2);
  double *TF_B = new double [stateSum];
  double *TF_D = new double [stateSum];
  //initialization
  for (int i = 0; i < stateSum; ++i)
  {
    TF_B[i] = 0;
    TF_D[i] = 0;
  }

  calDeltaBRec(stateNetwork,fixpoint,TF_B); //!!!!!
  calDeltaDRec(stateNetwork,fixpoint,TF_D);
  //statistics the checkpoint
  double *weight_b1 = new double[size];
  double *weight_b2 = new double[size];
  double *weight_b = new double[size];
  double *weight_d1 = new double[size];
  double *weight_d2 = new double[size];
  double *weight_d = new double[size];

  //initialization
  for (int i = 0; i < size; ++i)
  {
    weight_b1[i] = 0;
    weight_b2[i] = 0;
    weight_b[i] = 0;
    weight_d1[i] = 0;
    weight_d2[i] = 0;
    weight_d[i] = 0;
  }
  int state;
  //calculate the b
  start_state.reset();
  for(int i = 0; i < stateSum;i++){
    state = (int)start_state;
    if(TF_B[state] >= 1){
      for(int j = 0; j < size; j++){
	if(start_state[j] == 1)
	  weight_b1[j] += TF_B[state];
	else
	  weight_b2[j] -= TF_B[state];
	
	weight_b[j] += TF_B[state];
      }
    }
    start_state.nextSequence();
  }
  //calculate the d
  start_state.reset();
  for(int i = 0; i < stateSum;i++){
    state = (int)start_state;
    if(TF_D[state] >= 1){
      for(int j = 0; j < size; j++){
	if(start_state[j] == 1)
	  weight_d1[j] += TF_D[state];
	else
	  weight_d2[j] -= TF_D[state];
	
	weight_d[j] += TF_D[state];
      }
    }
    start_state.nextSequence();
  }

 // for(int j = 0; j < size; j++)
 //    std::cout<<(j+1)<<" "<<(weight_b1[j]+weight_b2[j])/weight_b[j]<<
 //      " "<<(weight_d1[j]+weight_d2[j])/weight_d[j]<<std::endl;

  double *bd = new double [size];
  for(int j = 0; j < size; ++j){
    bd[j] = ((weight_b1[j]+weight_b2[j])/weight_b[j]+
    	     (weight_d1[j]+weight_d2[j])/weight_d[j])/2;
    // bd[j] = ((weight_b1[j]+weight_b2[j])/weight_b[j]) *
    //   ((weight_d1[j]+weight_d2[j])/weight_d[j]);
  }
 
  // for(int i = 0; i < size; ++i){
  //   std::cout<<" "<<bd[i]<<" "<<std::endl;
  // }

  delete weight_b1;
  delete weight_b2;
  delete weight_b;
  delete weight_d1;
  delete weight_d2;
  delete weight_d;
  delete TF_B;
  delete TF_D;
  return bd;
}


/********************************************************************************************/

/**********************************define main function**************************************/

void mainfunction(int *network,int *minNetwork,int size,int fixpoint)
{
  //calculate the b w
  auto dn = genDN(network,size);
  auto bw = calmaxBW(dn); //76,272
  // std::cout<<"B = "<<bw.first<<" W = "<<bw.second<<std::endl;
  int total_state  = pow(2,size);
  //if((double)bw.first/total_state < 0.8) return;
  //calculate the supplement node
  int* supp_node = new int [size];
  for(int i = 0; i < size; ++i)
    supp_node[i] = 0;
  int *supp_network = subMatrix(network,minNetwork,size,size);
  for(int i = 0; i < size; ++i){
    for(int j = 0; j < size; ++j){
      if(supp_network[i*size+j] != 0){
	supp_node[j] ++; //outdegree!!!
	supp_node[i] ++; //indegree!!!
      }
    }
  }

  //calculate the b d
  int *node_num = new int[size];
  for(int i = 0; i < size; ++i){
    node_num[i] = i+1; //set node index
  }
  double *bd = calBD2(network,size,fixpoint);
  //pop sort
  for(int i = 0; i < size; ++i){
    for(int j = 0; j < size; ++j){
      if(bd[i]>bd[j]){
	swap2(bd[i],bd[j]);
	swap2(node_num[i],node_num[j]);
	swap2(supp_node[i],supp_node[j]);
      }
    }
  }

  //print the result
  // for(int i = 0; i < size; ++i){
  //   std::cout<<std::setw(2)<<node_num[i]<<" "<<std::setw(10)<<bd[i]<<" "<<supp_node[i]<<std::endl;
  // }
  //calculate the correlation
  double correlation = 0;
  double bd_total = 0;
  double indegree_total = 0;
  double unzero_num = 0;
  double deviation = 0;
  for(int i = 0; i < size-1; ++i){
    //bd_total += bd[i] * bd[i];
    indegree_total += supp_node[i] * supp_node[i];
    bd_total += abs2(bd[i]);
    //indegree_total += supp_node[i];
    if(supp_node[i] != 0) unzero_num += 1;
  }
  
  for(int i = 0; i < size-1; ++i){
    correlation += (abs2(bd[i])/sqrt(bd_total)) * (supp_node[i]/sqrt(indegree_total));
    deviation += supp_node[i] * supp_node[i];
  }
  // double para = 0;
  // for(int i = size-2; i > size -5; --i){
  //   para += abs2(bd[i])/bd_total + supp_node[i]/indegree_total;
  // }
  // std::cout<<"deviation = "<<sqrt(deviation)/(unzero_num*indegree_total)<<"\n"<<"correlation ="<<correlation<<std::endl;
  // std::cout<<sqrt(deviation)/(unzero_num*indegree_total)<<" "<<correlation<<std::endl;
  
  //std::cout<<bw.first<<" "<<correlation<<std::endl;
  //else std::cout<<0.1<<" "<<correlation<<std::endl;
  delete supp_node;
  delete node_num;
  delete bd;
}


/********************************************************************************************/



int main(int argc,char * argv[])
{
  if (argc != 5) //quit if not input "filename" "size"
  {
    std::cerr<<"Usage: "<<argv[0]<<" fullnetwork "<<" min-network "<<" nodeSize "<<" fixpoint "<<std::endl;
    exit(EXIT_FAILURE);
  }
  int nodeNum = atoi(argv[3]);
  int fixpoint = atoi(argv[4]);
  std::clock_t c_start = std::clock();
  
  int *network = genMatrix(argv[1],nodeNum,nodeNum);
  int *minNetwork = genMatrix(argv[2],nodeNum,nodeNum);

  mainfunction(network,minNetwork,nodeNum,fixpoint);
  // int genNetNum = 1000;
  // std::string *filenames_full = genStrList(genNetNum,"_full");
  // std::string *filenames_min = genStrList(genNetNum,"_min");
  // for(int i = 0; i < genNetNum; ++i){
  //   int *network = genMatrix(const_cast<char*>(filenames_full[i].c_str()),nodeNum,nodeNum);
  //   int *minNetwork = genMatrix(const_cast<char*>(filenames_min[i].c_str()),nodeNum,nodeNum);
  //   mainfunction(network,minNetwork,nodeNum);
  // }
  // std::clock_t c_end = std::clock();
  // std::cout<<"It costs : "<<(c_end-c_start)<<" ms."<<std::endl;
}
