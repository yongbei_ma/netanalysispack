/***************************************************************************
 *
 * Authors: "Yongbei(Glow) Ma"
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This complete copyright notice must be included in any revised version of the
 * source code. Additional authorship citations may be added, but existing
 * author citations must be preserved.
 ***************************************************************************/
/****give a network,then find its conjectory,calculate the relation
between the supplent node and para BD.
 **@author YongBei.Ma
 **@date 2013.09.30
 **@version 1.0.0
 *****/


#include <iostream>
#include <cstdlib>
#include <list>

#include "../header/basicmatrix.h"
#include "./checkpoint.cpp"
#include "../header/basicsequence.h"
#include "../header/findminnetwork.h"

int main(int argc, char *argv[])
{
  if (argc != 3){
    std::cerr<<"Usage: "<<argv[0]<<" network "<<" state "<<" size "<<" duration "<<std::endl;
    exit(EXIT_FAILURE);
  }
  int nodeNum = atoi(argv[2]);
  //int duration = atoi(argv[4]);
  std::list<int> trajectory;
  //srand((unsigned)time(NULL));
  for(int i = 0; i < 1; ++i){
    int *full = genMatrix(argv[1],nodeNum,nodeNum);
    //int *state = genMatrix(argv[2],nodeNum,duration);
    //int *full = genMatrix(nodeNum,nodeNum,-1,2);
    prtMatrix(full,nodeNum,nodeNum);
    std::cout<<std::endl;
    //int *min = sMinNetworkBFS(state,m,nodeNum,duration);
    //prtMatrix(min,nodeNum,nodeNum);
    auto dn = genDN(full,nodeNum);
    int attractor = findMaxAttractor(dn);
    findMainAttractor(dn,trajectory,attractor);
    int duration = trajectory.size();
    int *state = new int [nodeNum*duration];
    Sequence seq(nodeNum,2);
    int col = 0;
    while(!trajectory.empty()){
      seq.set(trajectory.back());
      trajectory.pop_back();
      std::cout<<seq<<std::endl;
      for(int i = 0; i < nodeNum; ++i)
	state[col*nodeNum+i] = seq[i];
      col++;
    }
    
    std::cout<<std::endl;
    int *min = sMinNetworkBFS(state,full,nodeNum,duration);
    prtMatrix(min,nodeNum,nodeNum);

    mainfunction(full,min,nodeNum,attractor);

    delete full;
    delete min;
    delete state;
  }
  return 0;
}
