/***************************************************************************
 *
 * Authors: "Yongbei(Glow) Ma"
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This complete copyright notice must be included in any revised version of the
 * source code. Additional authorship citations may be added, but existing
 * author citations must be preserved.
 ***************************************************************************/
/*****
@author YongBei Ma
@date 2013.3.29
@version 1.0
*****/
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <cmath>

#include <cstdlib>
#include <ctime>
#include <climits>

#include "../header/basicmatrix.h"
#include "../header/basicsequence_old.h"


//calculate the next end state 
void nextEndState(int *network,Sequence& start_state,Sequence& end_state,int size)
{
  int temp;
  /****basical model*****/
  // for(int i = 0;i < size;i++){//calculate S(i+1)
  //     temp = 0;     //result
  //     for(int j = 0;j < size;j++)
  //   	 if(start_state[j]) temp += network[i*size+j];
  //     end_state[i] = temp<=0?false:true;
  // }

  /****forbid domination model****/
  for(int i = 0;i < size;i++){//for every onde i
    temp = 0;     //result
    for(int j = 0;j < size;j++) //for all j
      if(start_state[j]){  //j is active
  	if(network[i*size+j] == -1){
  	  if(j != i) {
  	    temp = -100; //strong inhibitory,special case
  	    break;
  	     }
  	}
      	temp += network[i*size+j];
        
    }
// std::cout<<temp<<" ";
    if(temp < 0)
      end_state[i] = 0;
    else if(temp == 0 && network[i*size+i] != 1) //add && network[i*size+i] != 1 condition
      end_state[i] = start_state[i];
    else
      end_state[i] = true;
  }

  /******boolean model*******/
  // for(int i = 0;i < size;i++){//for every onde i
  //   temp = 0;     //result
  //   for(int j = 0;j < size;j++) //for all j
  //     if(start_state[j])
  //     	temp += network[i*size+j];
  //   if(temp < 0)
  //     end_state[i] = 0;
  //   else if(temp == 0 && network[i*size+i] != 1)  //add && network[i*size+i] != 1 condition
  //     end_state[i] = start_state[i];
  //   else
  //     end_state[i] = true;
  // }
}

//give a network,and generate its dynamics 
void genDynamics(int *network,int size,int init_state)
{
  bool set[2] = {false,true}; //it is useless
  Sequence start_state(size,2,init_state);
  Sequence end_state(size,2);

  int count = 0;

  for(int i = 0;i < 15;i++){
    // start_state.inverse();
    std::cout<<start_state<<" ."<<(int)start_state<<std::endl;
    // start_state.inverse();
    //calculate the next state
    nextEndState(network,start_state,end_state,size);
    if(start_state == end_state)
      break;
    else
      start_state = end_state;
    //count++;
  }
  //std::cout<<(count+1);
}

void prtStateTrans(int *network,int size,int init_state)
{
  Sequence start_state(size,2,init_state);
  Sequence end_state(size,2);
  nextEndState(network,start_state,end_state,size);
  // start_state.inverse();
  // end_state.inverse();
  std::cout<<((int)start_state+1)<<" "<<((int)end_state+1)<<" "<<"1.0"<<std::endl;
}

int main(int argc,char * argv[])
{
  if (argc != 4) //quit if not input "filename" "size"
  {
    std::cerr<<"Usage: "<<argv[0]<<" fullnetwork "<<" nodeSize "<<" start-state(decimalism) "<<std::endl;
    exit(EXIT_FAILURE);
  }
  int nodeNum = atoi(argv[2]);
  int start_state = atoi(argv[3]);
  std::clock_t c_start = std::clock();
  
  int *network = genMatrix(argv[1],nodeNum,nodeNum);
  std::cout<<"network : "<<std::endl;
  prtMatrix(network,nodeNum,nodeNum);
  std::cout<<"dynamical process : "<<" state  "<<std::endl;
  //prtMatrix(network,nodeNum,nodeNum);
  //Sequence seq(nodeNum,2);
  // for(int n = 0;n < 512;n++){
  //   seq.set(n);
  //   //std::cout<<n<<" ";
  //   genDynamics(network,nodeNum,seq);
  //   std::cout<<std::endl;
  // }
  genDynamics(network,nodeNum,start_state);

  std::clock_t c_end = std::clock();
  std::cout<<"It costs : "<<(c_end-c_start)<<" ms."<<std::endl;


  /*** print the *.net fiel for gephi read ***/
  int num_states = pow(2,nodeNum);
  std::cout<<"*Vertices"<<" "<<num_states<<std::endl;
  for (int i = 0; i < num_states; ++i)
  {
    std::cout<<(i+1)<<" \""<<(i+1)<<"\" "<<(double)rand()/(RAND_MAX)<<std::endl;
  }
  std::cout<<"*Arcs"<<std::endl;
  for (int i = 0; i < num_states; ++i)
  {
    prtStateTrans(network,nodeNum,i);
  }
}
