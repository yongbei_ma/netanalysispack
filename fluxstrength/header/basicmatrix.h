/***************************************************************************
 *
 * Authors: "Yongbei(Glow) Ma"
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This complete copyright notice must be included in any revised version of the
 * source code. Additional authorship citations may be added, but existing
 * author citations must be preserved.
 ***************************************************************************/
/*****this head file define some basic matrix operation
****author YongBei.Ma
****date 2013.3.29
****version 1.0
*****/
#ifndef BASICMATRIX_H_
#define BASICMATRIX_H_

#include <iostream>
#include <fstream>
#include <iomanip>

//generate a integer matrix from file
int * genMatrix(char * fileName,int row,int col)
{
  int * matrix = new int [row*col];
  std::ifstream inFile;
  inFile.open(fileName);
  if(!inFile.is_open()) //fail to open a file
  {
    std::cout<<"Could not open a file:"<<fileName<<std::endl;
    exit(EXIT_FAILURE);
  }
  int temp;
  for(int r = 0;r < row;r++)
  {
    for(int c = 0;c < col;c++)
    {
      inFile>>temp;
      matrix[r*col+c] = temp;
    }
  }
  inFile.close();
  return matrix;
}

//delete matrix
void delMatrix(int *M){delete [] M;}

//print a integer matrix
void prtMatrix(int *M,int row,int col)
{
  for(int r = 0;r < row;r++){
    for(int c = 0;c < col;c++)
      std::cout<<std::setw(4)<<M[r*col+c];
    std::cout<<std::endl;
  }
}

//copy matrix
void copyMatrix(int *from,int *to,int row,int col){
  int N = row * col;
  for(int i = 0;i < N;i++)
    to[i] = from[i];
}

//subtracting matrix
int * subMatrix(int *matrixA,int *matrixB,int row,int col)
{
  int * matrix = new int [row*col];
  for(int r = 0;r < row;r++)
    for(int c = 0;c < col;c++)
      matrix[r*col+c] = matrixA[r*col+c] - matrixB[r*col+c];
  return matrix;
}

//randomly generate a matrix filled with random number
int * genMatrix(int row,int col,int from,int to){
  int * matrix = new int [row*col];
  //srand((unsigned)time(NULL));
  for(int r = 0;r < row;r++)
    for(int c = 0;c < col;c++)
      matrix[r*col+c] = rand() % (to - from) + from;
  return matrix;
}

#endif
