/***************************************************************************
 *
 * Authors: "Yongbei(Glow) Ma"
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This complete copyright notice must be included in any revised version of the
 * source code. Additional authorship citations may be added, but existing
 * author citations must be preserved.
 ***************************************************************************/
/****find the given network's corresponding min-network,using brute force search approach
 **@author YongBei.Ma
 **@date 2013.10.01
 **@version 1.0.0
 *****/
#ifndef FINDMINNETWORK_H_
#define FINDMINNETWORK_H_

#include <iostream>
#include <cmath>

#include "../header/basicmatrix.h"
#include "../header/basicsequence.h"
#include "../header/helpfun.h"


/*******************************************************************************/

/********************find the appropriate interaction*********************************/
//check the given interaction wheather contents the dynamical-process 
bool logical_exp(MyInteraction &interaction ,int *state,int inNode,int periods,int nodeNum){
  for(int t = 0;t < periods-1;t++){
    bool left;
    if(interaction[inNode] != 1)//left expression,self degradation
      left = state[t*nodeNum+inNode] && (! interaction[inNode]);
    else
      left = true; /*! nodeState[t][toNode];*/     // self activation
    for(int j = 0;j < nodeNum;j++){
      if(j == inNode) continue;
      if(interaction[j] == -1 && state[t*nodeNum+j]){//right expression
	if(state[(t+1)*nodeNum+inNode])
	  return false;
	else{
	  left = state[(t+1)*nodeNum+inNode];  //make sure this time must be right
	  break;
	}	
      }
      left = left || (interaction[j] && state[t*nodeNum+j]);
    }
    if(left != state[(t+1)*nodeNum+inNode]) return false;
  }
  return true;
}

//using the brute force search to find the existent network
int* sMinNetworkBFS(int * state,int * fullNetwork,int nodeNum,int duration){
  MyInteraction seq(nodeNum);
  MyInteraction fullSeq(nodeNum);
  int allSeq = pow(3,nodeNum),seqMinLength;  //iterate all sequence
  int * minNetwork = new int [nodeNum*nodeNum];
  for(int n = 0;n < nodeNum;n++){ //for every onde
    for (int i = 0; i < nodeNum; ++i) //set the actual interaction
      fullSeq.set(i,fullNetwork[n*nodeNum+i]);
    seqMinLength = fullSeq.getLength();

    seq.reset();
    for(int t = 0;t < allSeq;t++){ //for every sequence
      if(seq.getLength() <= seqMinLength && fullSeq.isSubSeq(seq) && logical_exp(seq,state,n,duration,nodeNum)){
        for(int i = 0; i < nodeNum; ++i)
          minNetwork[n*nodeNum+i] = seq[i];
        seqMinLength = seq.getLength();
      }
      seq.nextSequence();
    }
  }
  return minNetwork;
}


#endif